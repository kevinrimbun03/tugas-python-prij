NPM_Siswa = int(input("Masukkan NPM Siswa: "))
Nama_Siswa = str(input("Masukkan Nama Siswa: "))
Nilai_Tugas1 = int(input("Masukkan Nilai Tugas ke - 1: "))
Nilai_Tugas2 = int(input("Masukkan Nilai Tugas ke - 2: "))
Nilai_Tugas3 = int(input("Masukkan Nilai Tugas ke - 3: "))

rata_rata = (Nilai_Tugas1 + Nilai_Tugas2 + Nilai_Tugas3) / 3
huruf_mutu = ["A", "B", "C", "D", "E"]
angka_mutu = ["4", "3", "2", "1", "0"]

if rata_rata >= 80:
    print("NPM", NPM_Siswa, "Mahasiswa yang bernama", Nama_Siswa, "Memperoleh nilai rata-rata", rata_rata, "yang berasal dari tugas yang dikumpulkannya, sehingga mendapatkan huruf mutu akhir sebesar", huruf_mutu[0], "dan angka mutu sebesar", angka_mutu[0])
elif rata_rata >= 68:
    print("NPM", NPM_Siswa, "Mahasiswa yang bernama", Nama_Siswa, "Memperoleh nilai rata-rata", rata_rata, "yang berasal dari tugas yang dikumpulkannya, sehingga mendapatkan huruf mutu akhir sebesar", huruf_mutu[1], "dan angka mutu sebesar", angka_mutu[1])
elif rata_rata >= 56:
    print("NPM", NPM_Siswa, "Mahasiswa yang bernama", Nama_Siswa, "Memperoleh nilai rata-rata", rata_rata, "yang berasal dari tugas yang dikumpulkannya, sehingga mendapatkan huruf mutu akhir sebesar", huruf_mutu[2], "dan angka mutu sebesar", angka_mutu[2])
elif rata_rata >= 45:
    print("NPM", NPM_Siswa, "Mahasiswa yang bernama", Nama_Siswa, "Memperoleh nilai rata-rata", rata_rata, "yang berasal dari tugas yang dikumpulkannya, sehingga mendapatkan huruf mutu akhir sebesar", huruf_mutu[3], "dan angka mutu sebesar", angka_mutu[3])
elif rata_rata < 45:
    print("NPM", NPM_Siswa, "Mahasiswa yang bernama", Nama_Siswa, "Memperoleh nilai rata-rata", rata_rata, "yang berasal dari tugas yang dikumpulkannya, sehingga mendapatkan huruf mutu akhir sebesar", huruf_mutu[4], "dan angka mutu sebesar", angka_mutu[4])