nama_pegawai = str(input("Masukkan Nama Pegawai: "))
gaji_pokok = int(input("Masukkan Gaji Pokok Pegawai: "))
jumlah_anak = int(input("Masukkan Jumlah Anak: "))


if gaji_pokok <= 500000:
    tunjangan_anak = 25000 * jumlah_anak
    pajak = gaji_pokok * 0 / 100
    persen = "0%"
    zakat = "2.5%"
    gaji_bersih = (gaji_pokok * 97.5 / 100) + 50000 + tunjangan_anak
    print("Gaji Bersih yang diterima Pegawai A.N.", nama_pegawai.title(),"adalah Rp.", gaji_bersih, "dengan potongan pajak sebesar", persen, "dan zakat sebesar", zakat)
elif gaji_pokok <= 1000000:
    tunjangan_anak = 25000 * jumlah_anak
    pajak = gaji_pokok * 0 / 100
    persen = "5%"
    zakat = "2.5%"
    gaji_bersih = (gaji_pokok * 95 / 100) + 50000 + tunjangan_anak
    print("Gaji Bersih yang diterima Pegawai A.N.", nama_pegawai.title(),"adalah Rp.", gaji_bersih, "dengan potongan pajak sebesar", persen, "dan zakat sebesar", zakat)
elif gaji_pokok > 1000000:
    tunjangan_anak = 25000 * jumlah_anak
    pajak = gaji_pokok * 0 / 100
    persen = "5%"
    zakat = "2.5%"
    gaji_bersih = (gaji_pokok * 92.5 / 100) + 50000 + tunjangan_anak
    print("Gaji Bersih yang diterima Pegawai A.N.", nama_pegawai.title(),"adalah Rp.", gaji_bersih, "dengan potongan pajak sebesar", persen, "dan zakat sebesar", zakat)